#include <Eigen/Dense>

Eigen::VectorXcd appendVectors(const Eigen::VectorXcd& vec1, const Eigen::VectorXcd& vec2);

void insertMatrix(Eigen::MatrixXcd& mainMatrix, const Eigen::MatrixXcd& subMatrix, int startRow);