#include "VectorHelpers.h"

Eigen::VectorXcd appendVectors(const Eigen::VectorXcd& vec1, const Eigen::VectorXcd& vec2) {
    Eigen::VectorXcd result(vec1.size() + vec2.size());
    result << vec1, vec2;
    return result;
}

void insertMatrix( Eigen::MatrixXcd& mainMatrix, const Eigen::MatrixXcd& subMatrix, int startRow) {
    mainMatrix.block(startRow, 0, subMatrix.rows(), mainMatrix.cols()) = subMatrix;
}