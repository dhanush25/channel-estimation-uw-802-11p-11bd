find_package(GTest REQUIRED)

set(UNITTESTS unit-tests)
add_executable(${UNITTESTS})

set(TESTS
        test_OFDM.cpp
        )

target_sources(${UNITTESTS}
        PRIVATE
        ${TESTS}
        )

target_link_libraries(${UNITTESTS} PRIVATE
        GTest::gtest_main
        ${PROJECT_NAME}
)

target_link_libraries(${UNITTESTS} PUBLIC
        fftw::fftw
        Eigen3::Eigen
        fmt::fmt
        spdlog::spdlog)   # Link against main project's liṭbrariesṭ