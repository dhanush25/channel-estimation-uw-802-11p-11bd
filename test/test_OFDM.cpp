#include <gtest/gtest.h>
#include <Eigen/Dense>

#include "../src/Constants.h"
#include "../src/GenerateUniqueWord.h"
#include "../src/GenerateOFDMPayload.h"

TEST(TEST_UNIQEWORD_SIZE, OFDM) {
    GenerateUniqueWord uniqueWord;
    Eigen::VectorXcd UWInTime = uniqueWord.getUniqueWordSequence();
    ASSERT_EQ(UWInTime.size(), 32);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}