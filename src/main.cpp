#include <iostream>

#include "FourierTransform.h"
#include "GenerateOFDMPayload.h"

int main()
{
    GenerateOFDMPayload payloadOFDM(192, 3); // pre defined seed for testing
    payloadOFDM.generatePayload();
}
