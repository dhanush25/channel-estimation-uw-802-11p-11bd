#pragma once
#include <iostream>
#include <Eigen/Dense>

#include "Constants.h"

class GenerateUniqueWord
{
public:
    GenerateUniqueWord();
    Eigen::VectorXcd getUniqueWordSequence();
    Eigen::VectorXcd calculateZadOffChuSeq(int, int);
    void displayUniqueWord();
private:
    Eigen::VectorXcd UWInTime;
};
