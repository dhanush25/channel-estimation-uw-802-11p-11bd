#include <vector>
#include <random>
#include <optional>
#include <Eigen/Dense>

#include "Constants.h"
#include "GenerateUniqueWord.h"
#include "FourierTransform.h"
#include "../utils/VectorHelpers.h"
#include "QAM.h"

class GenerateOFDMPayload {
public:
    explicit GenerateOFDMPayload(uint32_t N, std::optional<uint32_t> seedForRandomBitStreamGeneration = std::nullopt);
    std::vector<bool> generateRandomBits(std::vector<bool>&);
    Eigen::MatrixXcd generatePayload();
    Eigen::MatrixXcd performOFDMModulation();
    Eigen::MatrixXcd insertUniqueWord();

    Eigen::VectorXcd getUniqueWord();
    Eigen::MatrixXcd getPayload();
    void displayPayload();

private:
    uint32_t N;
    std::mt19937 gen;
    Eigen::MatrixXcd payload;
    Eigen::VectorXcd UWinTime;
    Eigen::MatrixXcd payloadBeforeIFFT;
    Eigen::MatrixXcd payloadAfterIFFT;
    Eigen::MatrixXcd payloadWithUniqueWord;
};
