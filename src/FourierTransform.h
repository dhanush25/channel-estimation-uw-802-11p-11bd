#pragma once

#include <fftw3.h>
#include <Eigen/Dense>

#define REAL 0
#define IMAG 1

class FourierTransform {
private:
    int size;
    fftw_complex *in;
    fftw_complex *out;
    fftw_plan forwardPlan;
    fftw_plan inversePlan;

public:
    explicit FourierTransform(int N);
    ~FourierTransform() ;
    void forward();
    void inverse();
    void setInput(const Eigen::VectorXcd&);
    Eigen::VectorXcd getOutputAsVector();
};