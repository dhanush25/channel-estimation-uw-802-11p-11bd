#include "GenerateUniqueWord.h"

GenerateUniqueWord::GenerateUniqueWord() {
    UWInTime = Eigen::VectorXcd::Zero(UniqueWordParameters::UNIQUE_WORD_LENGTH, GeneralParameters::nT);
}

Eigen::VectorXcd GenerateUniqueWord::getUniqueWordSequence() {
    for (int i = 0; i <= GeneralParameters::nT; i++) {
        UWInTime = calculateZadOffChuSeq(1, UniqueWordParameters::UNIQUE_WORD_LENGTH);
    }
    return UWInTime;
}

// Check: https://de.mathworks.com/help/comm/ref/zadoffchuseq.html
// SEQ = ZADOFFCHUSEQ(R,N) generates the Rth root Zadoff-Chu sequence of
// length N. The output SEQ is an N-length column vector of complex
// symbols.
Eigen::VectorXcd GenerateUniqueWord::calculateZadOffChuSeq(int root, int len) {
    Eigen::VectorXcd seq(len);
    for (int n = 0; n < len; ++n) {
        double arg = M_PI * root * n * (n + 1) / len;
        seq(n) = std::exp(std::complex<double>(0, -arg));
    }
    return seq;
}

void GenerateUniqueWord::displayUniqueWord() {
    for (int i = 0; i < UWInTime.size(); ++i) {
        std::cout << "Element " << i << ": " << UWInTime(i).real() << " + " << UWInTime(i).imag() << "i" << std::endl;
    }
}

