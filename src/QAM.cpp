#include "QAM.h"


QAM::QAM(int M) : M(M)
{
    numBitsPerSymbol = static_cast<int>(log2(M));
}

Eigen::VectorXcd QAM::modulate(const std::vector<bool> &bitStream)
{
    int numSymbols = bitStream.size() / numBitsPerSymbol;
    Eigen::VectorXcd modulatedSymbols(numSymbols);

    for (int i = 0; i < numSymbols; ++i)
    {
        int symbolIndex = 0;
        // Convert binary bits to symbol index
        for (int j = 0; j < numBitsPerSymbol; ++j)
        {
            symbolIndex += bitStream[i * numBitsPerSymbol + j] * (1 << (numBitsPerSymbol - 1 - j));
        }
        modulatedSymbols[i] =  performQAMMapping(symbolIndex);
    }

    return modulatedSymbols;
}

std::complex<double> QAM::performQAMMapping(int symbolIndex)
{
    if (M <= 0) {
        std::cerr << "Error: M should be greater than 0" << std::endl;
        return {0, 0};
    }

    // Check if symbolIndex is within range
    if (symbolIndex < 0 || symbolIndex >= M) {
        std::cerr << "Error: symbolIndex is out of range" << std::endl;
        return {0, 0};
    }

    // Compute real and imaginary parts
    double realPart = 2 * (symbolIndex % static_cast<int>(std::sqrt(M))) - std::sqrt(M) + 1;
    double imagPart = 2 * (symbolIndex / static_cast<int>(std::sqrt(M))) - std::sqrt(M) + 1;

    // Scale real and imaginary parts
    realPart /= (std::sqrt(M) - 1);
    imagPart /= (std::sqrt(M) - 1);

    return {realPart, imagPart};
}
