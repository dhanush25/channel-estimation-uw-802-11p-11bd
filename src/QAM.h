#pragma once

#include <iostream>
#include <cmath>
#include <complex>
#include <vector>
#include <random>
#include <Eigen/Dense>

class QAM {
public:
    explicit QAM(int);
    Eigen::VectorXcd modulate(const std::vector<bool> &);
    std::complex<double> performQAMMapping(int);

private:
    int M{};
    int numBitsPerSymbol{};
};
