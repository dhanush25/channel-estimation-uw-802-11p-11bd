#include "GenerateOFDMPayload.h"

GenerateOFDMPayload::GenerateOFDMPayload(uint32_t N, std::optional<uint32_t> seedForRandomBitStreamGeneration)
    : N(N)
{
    if (seedForRandomBitStreamGeneration.has_value())
    {
        gen.seed(seedForRandomBitStreamGeneration.value());
    }
    else
    {
        std::random_device rd;
        gen.seed(rd());
    }
    payload = Eigen::MatrixXcd::Zero(OFDMParameters::N_data, GeneralParameters::noBlk);
    payloadBeforeIFFT = Eigen::MatrixXcd::Zero(OFDMParameters::N, GeneralParameters::noBlk);
    payloadAfterIFFT = Eigen::MatrixXcd::Zero(OFDMParameters::N, GeneralParameters::noBlk);
    payloadWithUniqueWord = Eigen::MatrixXcd::Zero(OFDMParameters::N + UniqueWordParameters::UNIQUE_WORD_LENGTH, GeneralParameters::noBlk);
}

std::vector<bool> GenerateOFDMPayload::generateRandomBits(std::vector<bool>& bits)
{

    std::uniform_int_distribution<> dis(0, 1);
    for (uint32_t i = 0; i < N; ++i)
    {
        bits.push_back(dis(gen));
    }
    return bits;
}

Eigen::MatrixXcd GenerateOFDMPayload::generatePayload()
{
    for(int i = 0; i < GeneralParameters::noBlk; i++)
    {
        std::vector<bool> bits;
        generateRandomBits(bits);
        QAM qam(pow(2, GeneralParameters::mu));
        payload.col(i) = qam.modulate(bits);
    }
    // Subcarrier allocation
    insertMatrix(payloadBeforeIFFT, payload, 6);

    // perform IFFT
    performOFDMModulation();
    return payloadBeforeIFFT;
}

Eigen::MatrixXcd GenerateOFDMPayload::insertUniqueWord()
{
    getUniqueWord();
    return payloadWithUniqueWord;

}

Eigen::VectorXcd GenerateOFDMPayload::getUniqueWord()
{
    GenerateUniqueWord uniqueWord;
    UWinTime = uniqueWord.getUniqueWordSequence();
    return UWinTime;
}

void GenerateOFDMPayload::displayPayload()
{
    std::cout<< payloadAfterIFFT.rows() << " x " << payloadAfterIFFT.cols()<<std::endl;
    std::cout << "payloadAfterIFFT:\n" << payloadAfterIFFT << std::endl;
}

Eigen::MatrixXcd GenerateOFDMPayload::getPayload() {
    return payload;
}

Eigen::MatrixXcd GenerateOFDMPayload::performOFDMModulation() {
    for(int i = 0; i < GeneralParameters::noBlk; i++)
    {
        FourierTransform ifftw(OFDMParameters::N);
        ifftw.setInput( payloadBeforeIFFT.col(i));
        ifftw.inverse();
        payloadAfterIFFT.col(i) = ifftw.getOutputAsVector();
    }
}
