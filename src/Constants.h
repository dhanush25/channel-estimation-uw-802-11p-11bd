#pragma once
#include <cstdint>

namespace GeneralParameters
{
    constexpr int nT = 1; // number of Tx antennas
    constexpr int nR = 1; // number of Rx antennas
    constexpr int mu = 4; // Modulation order 2^mu-QAM
    constexpr int noBlk = 120; // number of blocks (OFDM symbols)
}// GENERAL_Parameters

namespace OFDMParameters
{
    constexpr uint16_t N = 64;  // FFT size
    constexpr uint16_t N_data = 48;  // Number of data subcarriers
    constexpr uint16_t Np = 4;     // Number of pilot subcarriers
    constexpr uint32_t SAMPLING_FREQUENCY = 10000000; // Sampling frequency
    constexpr double SHORT_CYCLIC_PREFIX = 1.6e-6;
    constexpr double N_Ncp = SHORT_CYCLIC_PREFIX * SAMPLING_FREQUENCY;
}// OFDM_Parameters

namespace UniqueWordParameters
{
    constexpr double UNIQUE_WORD_LENGTH = OFDMParameters::N_Ncp * ( GeneralParameters::nT + 1);
    constexpr double N_Nuw = UNIQUE_WORD_LENGTH + OFDMParameters::N;
    constexpr double FRAME_LENGTH_UW = GeneralParameters::noBlk * N_Nuw;
}//UNIQUE_WORD_PARAMTERS

