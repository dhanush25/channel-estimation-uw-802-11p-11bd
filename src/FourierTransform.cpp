#include "FourierTransform.h"
#include <iostream>

FourierTransform::FourierTransform(int N) : size(N) {
    in = new fftw_complex[size];
    out = new fftw_complex[size];
    forwardPlan = fftw_plan_dft_1d(size, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    inversePlan = fftw_plan_dft_1d(size, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
}

FourierTransform::~FourierTransform() {
    fftw_destroy_plan(forwardPlan);
    fftw_destroy_plan(inversePlan);
    delete[] in;
    delete[] out;
    fftw_cleanup();
}

void FourierTransform::setInput(const Eigen::VectorXcd& data) {
    if (data.rows() != size) {
        throw std::invalid_argument("Input data size does not match Fourier transform size.");
    }
    for (int i = 0; i < size; ++i) {
        in[i][REAL] = data[i].real(); // Real part
        in[i][IMAG] = data[i].imag(); // Imaginary part
    }
}

Eigen::VectorXcd FourierTransform::getOutputAsVector() {
    Eigen::VectorXcd result(size, 1);
    for (int i = 0; i < size; ++i) {
        result[i]= std::complex<double>(out[i][REAL], out[i][IMAG]);
    }
    return result;
}

void FourierTransform::forward() {
    fftw_execute(forwardPlan);
}

void FourierTransform::inverse() {
    fftw_execute(inversePlan);
}