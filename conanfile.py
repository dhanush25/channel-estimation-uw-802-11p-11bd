from conan import ConanFile
from conan.tools.cmake import cmake_layout


class ExampleRecipe(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeDeps", "CMakeToolchain"

    def requirements(self):
        self.requires("fftw/3.3.10")
        self.requires("eigen/3.4.0")
        self.requires("fmt/10.2.1")
        self.requires("spdlog/1.13.0")
        self.requires("gtest/1.14.0")

    def layout(self):
        cmake_layout(self)